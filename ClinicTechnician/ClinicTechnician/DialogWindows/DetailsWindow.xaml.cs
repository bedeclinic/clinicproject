﻿using System.Windows;
using ClinicTechnician.Communication;

namespace ClinicTechnician.DialogWindows
{
    /// <summary>
    /// Interaction logic for DetailsWindow.xaml
    /// </summary>
    public partial class DetailsWindow : Window
    {
        public DetailsWindow()
        {
            InitializeComponent();
        }

        public DetailsWindow(ExaminationData data): this()
        {
            PatientName.Content = data.PatientFirstName;
            PatientLastName.Content = data.PatientLastName;
            EntryDate.Content = data.EntryDate;
            ExecutionDate.Content = data.ExecutionDate;
            AcceptanceDate.Content = data.AcceptanceDate;
            State.Content = data.State;
            Code.Content = data.Code;
            PerformedBy.Content = data.Technician;
            AcceptedBy.Content = data.CTechnician;
            Result.Content = data.Result;
            Comment.Content = data.Comment;
            Description.Content = data.Description;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
