﻿using System.Windows;
using ClinicTechnician.Communication;

namespace ClinicTechnician.DialogWindows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public bool ReturnedValue { get; set; }
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (DatabaseConnection.Validate(loginBox.Text,passBox.Password))
            {
                ReturnedValue = true;
                this.Close();
            }
            else
            {
                infoLabel.Content = "Incorrect password or login";
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            ReturnedValue = false;
            this.Close();
        }
    }
}
