﻿using System.Windows;

namespace ClinicTechnician.DialogWindows
{
    /// <summary>
    /// Interaction logic for ExaminationWindow.xaml
    /// </summary>
    public partial class ExaminationWindow : Window
    {
        public bool ResultValue { get; set; }
        public string  Result { get; set; }
        public ExaminationWindow()
        {
            InitializeComponent();
        }

        private void bacButton_Click(object sender, RoutedEventArgs e)
        {
            ResultValue = false;
            this.Close();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Result = resultBox.Text;
            ResultValue = true;
            this.Close();
        }
    }
}
