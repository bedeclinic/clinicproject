﻿using System.Windows;

namespace ClinicTechnician.DialogWindows
{
    /// <summary>
    /// Interaction logic for ExaminationWindow.xaml
    /// </summary>
    public partial class CancelationWindow : Window
    {
        public bool ResultValue { get; set; }
        public string  Comment { get; set; }
        public CancelationWindow()
        {
            InitializeComponent();
        }

        private void bacButton_Click(object sender, RoutedEventArgs e)
        {
            ResultValue = false;
            this.Close();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            Comment = commentBox.Text;
            ResultValue = true;
            this.Close();
        }
    }
}
