﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicTechnician.Communication
{
    
    public class ExaminationData
    {
        public enum StateType
        {
            CanceledByTechnician, CanceledByChief, Done, Pending, Approved
        }
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public DateTime? AcceptanceDate { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public string Comment { get; set; }
        public StateType State { get; set; }
        public String Technician { get; set; }
        public String CTechnician { get; set; }

        public String PatientFirstName { get; set; }
        public String PatientLastName { get; set; }
    }
}
