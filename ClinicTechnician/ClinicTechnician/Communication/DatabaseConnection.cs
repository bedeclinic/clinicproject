﻿using ClinicTechnician.DialogWindows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;

namespace ClinicTechnician.Communication
{
    static class DatabaseConnection
    {
        public static Workers Worker { get; set; }
        public static bool IsChief { get; set; }

        private static readonly Entities EntityData = new Entities();

        public static bool Validate(string login, string password)
        {
            try
            {
                var userLogin = (from l in EntityData.AspNetUsers
                                where l.UserName == login
                                select l).Single();
                if (userLogin != null)
                {
                    String role = userLogin.AspNetRoles.Single().Name;
                    if (role == "ChiefTechnician" || role == "Technician")
                    {
                        if (Crypto.VerifyHashedPassword(userLogin.PasswordHash, password))
                        {
                            if (role == "ChiefTechnician")
                                IsChief = true;
                            var worker = EntityData.Workers.FirstOrDefault(w => w.FirstName == userLogin.FirstName && w.LastName == userLogin.LastName);
                            Worker = worker;
                            return true;
                        }
                            
                    }
                }
                return false;
            }
            catch (ArgumentNullException)
            {
                return false;
            }
            catch (InvalidOperationException)
            {
                return false;
            }

        }

        public static void PerformAction(ExaminationData data)
        {
            if (IsChief)
                ApproveExamination(data);
            else
                PerformExamination(data);

        }
        public static void ApproveExamination(ExaminationData data)
        {

            var entData = (from d in EntityData.MedicalExaminations
                           where d.Id == data.Id
                           select d).SingleOrDefault();
            data.State = ExaminationData.StateType.Approved;
            data.AcceptanceDate = DateTime.Now;
            data.CTechnician = String.Format("{0} {1}",Worker.FirstName,Worker.LastName);
            entData.ChiefTechnicianId = Worker.Id;
            entData.AcceptanceDate = data.AcceptanceDate;
            entData.State = (int?)data.State;
            EntityData.SaveChanges();
            
        }

        public static void PerformExamination(ExaminationData data)
        {
            ExaminationWindow exam = new ExaminationWindow();
            exam.ShowDialog();
            if (exam.ResultValue)
            {
                var entData = (from d in EntityData.MedicalExaminations
                               where d.Id == data.Id
                               select d).SingleOrDefault();
                data.State = ExaminationData.StateType.Done;
                data.Result = exam.Result;
                data.Technician = String.Format("{0} {1}", Worker.FirstName, Worker.LastName); ;
                data.ExecutionDate = DateTime.Now;
                entData.State = (int?)data.State;
                entData.ExecutionDate = data.ExecutionDate;
                entData.LaboratoryTechnicianId = Worker.Id;
                entData.Result = data.Result;
                EntityData.SaveChanges();
            }
        }

        public static void CancelExamination(ExaminationData data)
        {
            CancelationWindow cancel = new CancelationWindow();
            cancel.ShowDialog();
            if (cancel.ResultValue)
            {
                var entData = (from d in EntityData.MedicalExaminations
                               where d.Id == data.Id
                               select d).SingleOrDefault();
                if (IsChief)
                {
                    data.State = ExaminationData.StateType.CanceledByChief;
                    data.CTechnician = String.Format("{0} {1}", Worker.FirstName, Worker.LastName);
                    data.AcceptanceDate = DateTime.Now;
                    entData.ChiefTechnicianId = Worker.Id;
                }
                else
                {
                    data.State = ExaminationData.StateType.CanceledByTechnician;
                    data.Technician = String.Format("{0} {1}", Worker.FirstName, Worker.LastName);
                    data.ExecutionDate = DateTime.Now;
                    entData.LaboratoryTechnicianId = Worker.Id;
                }

                entData.ExecutionDate = data.ExecutionDate;
                entData.State = (int?)data.State;
                EntityData.SaveChanges();
            }
        }

        public static Tuple<bool, bool> CheckAvalibleOperations(object o)
        {
            ExaminationData data = o as ExaminationData;
            try
            {
                if (IsChief)
                {
                    if (data.State == ExaminationData.StateType.Pending)
                        return new Tuple<bool, bool>(false, true);
                    if (data.State == ExaminationData.StateType.Done)
                        return new Tuple<bool, bool>(true, true);
                }
                else if (data.State == ExaminationData.StateType.Pending)
                    return new Tuple<bool, bool>(true, true);
                return new Tuple<bool,bool>(false,false);
            }
            catch (NullReferenceException e)
            {
            }
            return null;
        }

        public static List<ExaminationData> SelectAll()
        {
            var dataList = (EntityData.MedicalExaminations.Join(EntityData.Visits, d => d.VisitId, v => v.Id,
                (d, v) => new {d, v})
                .Join(EntityData.Patients, @t => @t.v.PatientId, p => p.Id, (@t, p) => new {@t, p})
                .Where(@t => @t.@t.d.Type == 1)
                .Select(@t => new ExaminationData
                {
                    Id = @t.@t.d.Id,
                    Code = @t.@t.d.Code,
                    EntryDate = @t.@t.d.EntryDate,
                    ExecutionDate = @t.@t.d.ExecutionDate,
                    AcceptanceDate = @t.@t.d.AcceptanceDate,
                    Description = @t.@t.d.Description,
                    Result = @t.@t.d.Result,
                    Comment = @t.@t.d.Comment,
                    CTechnician = @t.@t.d.Workers1.FirstName +" "+ @t.@t.d.Workers1.LastName,
                    Technician = @t.@t.d.Workers.FirstName +" "+ @t.@t.d.Workers.LastName,
                    PatientFirstName = @t.p.FirstName,
                    PatientLastName = @t.p.LastName,
                    State = (ExaminationData.StateType) @t.@t.d.State
                })).ToList();

            return dataList;
        }

        public static bool Compare(string firstName, string lastName, DateTime? date, string state, ExaminationData data)
        {
            if (data.State.ToString() == state || state == "All")
            {
                bool dateCondition = true;
                try
                {
                    if (date.HasValue)
                    {
                        dateCondition = date.Value.Date.Equals(data.EntryDate.Value.Date);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                return data.PatientFirstName.ContainsString(firstName) && data.PatientLastName.ContainsString(lastName) &&
                    dateCondition;
            }
            return false;
        }

        #region extensionmethods
        public static bool ContainsString(this string source, string toCheck)
        {
            if(!string.IsNullOrEmpty(toCheck))
                return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
            return true;
        }
        #endregion
    }

}
