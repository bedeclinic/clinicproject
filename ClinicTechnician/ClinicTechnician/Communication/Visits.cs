//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClinicTechnician.Communication
{
    using System;
    using System.Collections.Generic;
    
    public partial class Visits
    {
        public Visits()
        {
            this.MedicalExaminations = new HashSet<MedicalExaminations>();
        }
    
        public int Id { get; set; }
        public System.DateTime RegistryDate { get; set; }
        public Nullable<int> State { get; set; }
        public Nullable<System.DateTime> ExecuteDate { get; set; }
        public string Description { get; set; }
        public string Diagonose { get; set; }
        public int PatientId { get; set; }
        public int RegistrantId { get; set; }
        public int DoctorId { get; set; }
    
        public virtual ICollection<MedicalExaminations> MedicalExaminations { get; set; }
        public virtual Patients Patients { get; set; }
        public virtual Workers Workers { get; set; }
        public virtual Workers Workers1 { get; set; }
    }
}
