﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ClinicTechnician.Communication;

namespace ClinicTechnician.DialogWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static RoutedCommand command = new RoutedCommand();
        public MainWindow()
        {
            InitializeComponent();
            command.InputGestures.Add(new KeyGesture(Key.F1));
            CommandBindings.Add(new CommandBinding(command, ShowHelp));
        }

        private void FillGrid()
        {
            CollectionViewSource src = this.Resources["CvsExamination"] as CollectionViewSource;
            if(src!=null)
            {
                src.Source = DatabaseConnection.SelectAll();
            }
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            LoginWindow login = new LoginWindow();
            login.ShowDialog();
            if (login.ReturnedValue)
            {
                HelloLabel.Content = string.Format("Hello {0} {1}!", DatabaseConnection.Worker.FirstName, DatabaseConnection.Worker.LastName);
                FillGrid();
                if (DatabaseConnection.IsChief)
                {
                    ActionButton.Content = "Approve";
                    FilterComboBox.SelectedIndex = 2;
                }
                else
                    ActionButton.Content = "Perform";
            }
            else
                this.Close();
        }

        private void actionButton_Click(object sender, RoutedEventArgs e)
        {
            DatabaseConnection.PerformAction(ExaminationsGrid.Items[ExaminationsGrid.SelectedIndex] as ExaminationData);
            ExaminationsGrid.Items.Refresh();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (ExaminationsGrid.SelectedIndex > -1)
            {
                DatabaseConnection.CancelExamination(ExaminationsGrid.Items[ExaminationsGrid.SelectedIndex] as ExaminationData);
                ExaminationsGrid.Items.Refresh();
            }
        }

        private void examinationsGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Code" || e.PropertyName == "EntryDate" || e.PropertyName == "ExecutionDate" ||
                e.PropertyName == "AcceptanceDate" || e.PropertyName == "PatientFirstName" || e.PropertyName == "PatientLastName" || e.PropertyName == "State")
                e.Cancel = false;
            else
            {
                e.Cancel = true;
            }
        }

        private void examinationsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var result = DatabaseConnection.CheckAvalibleOperations(ExaminationsGrid.Items[ExaminationsGrid.SelectedIndex]);
                ActionButton.IsEnabled = result.Item1;
                CancelButton.IsEnabled = result.Item2;
            }
            catch(ArgumentOutOfRangeException)
            {
                e.Handled = true;
            }
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            ExaminationData data = e.Item as ExaminationData;

            try
            {
                if(data!=null)
                {
                    ComboBoxItem item = FilterComboBox.SelectedItem as ComboBoxItem;
                    e.Accepted = DatabaseConnection.Compare(FirstNameBox.Text, LastNameBox.Text, DateBox.SelectedDate, item.Content.ToString(), data);
                }
            }
            catch (NullReferenceException)
            {
            }
        }

        private void filterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                CollectionViewSource.GetDefaultView(ExaminationsGrid.ItemsSource).Refresh();
            }
            catch (NullReferenceException)
            {
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CollectionViewSource.GetDefaultView(ExaminationsGrid.ItemsSource).Refresh();
            }
            catch (NullReferenceException)
            {
            }
        }

        private void ShowHelp(object sender, ExecutedRoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("Resources\\TechnicianHelp.chm");
        }
    }
}
