﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ClinicTechnician.Communication;
using ClinicTechnician.DialogWindows;

namespace ClinicTechnician.DialogWindows
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExaminationData data = ((DataGridRow) sender).Item as ExaminationData;
            try
            {
                DetailsWindow details = new DetailsWindow(data);
                details.ShowDialog();
            }
            catch (Exception)
            {
            }
        }
    }
}
